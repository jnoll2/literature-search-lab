#define(colname)(_#1_)
---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview


This deliverable focuses on the actual literature search part of your
review.  You will execute your search using the search
string you developed for the last deliverable, download the results, then apply your
inclusion and exclusion criteria.

# Instructions


1.  Read Section 6.2 "Study Selection" of Kitchenham &
    Charters' "Guidelines for performing systematic literature reviews
    in software engineering".

2.  Read Part B, item 3 "Literature search" of the
    coursework specification, to be sure you understand the requirements
    for this part of the coursework.

3.  Clone your group's Bitbucket repository.

4. Revise your #filename(research_question.yml) and/or #filename(review_protocol.yml)
   to account for any feedback you have received.
   
4.  Execute your (revised) search in _IEEEXplore_, using the "Command
Search" field of the Advanced Search facility.

5.  Download the results as a CSV file, using the _Export_ dropdown.

6.  Copy the CSV file into your repository, as #filename(papers.csv).
_IMPORTANT:_ you _must_ name the file #filename(papers.csv) exactly, else our
marking scripts will not find it, and you will receive **ZERO CREDIT
FOR THIS DELIVERABLE**.

7.  Add _five_ column names to #filename(papers.csv), as follows:

    * Column AE: #colname(include title)
    * Column AF: #colname(include abstract)
    * Column AG: #colname(exclude)
    * Column AH: #colname(accept contents)
    * Column AI: #colname(rq answers)

    See below for an example.

    ![papers.csv](papers.csv.png) \

8. Compare the title of _each_ and _every_ paper in #filename(papers.csv) to
your inclusion criteria.  If the title indicates the paper clearly
does not meet your _inclusion criteria_, write "no" in column AE
(#colname(include title)).  Conversely, if the title indicates the paper meets
your inclusion criteria, or you are not sure, write "yes" in the
#colname(include title) column.

9. Compare the abstract of _each_ and _every_ paper in #filename(papers.csv),
where there is a "yes" in the #colname(include title) column (_AE_), to
your inclusion criteria.  If the abstract indicates the paper clearly
does not meet your _inclusion criteria_, write "no" in column _AF_
(#colname(include abstract)).  Conversely, if the abstract indicates the paper meets
your inclusion criteria, or you are not sure, write "yes" in the
#colname(include abstract) column.

    Here are the allowable combinations of answers in these two
    columns:

    include title   include abstract 
   --------------- ------------------
         yes           yes           
         no          (blank)         
         yes           yes           
         yes           no            
         no            no            
         no          (blank)         

    The following combinations of answers in these two
    columns would be incorrect:

    include title   include abstract 
   --------------- ------------------
         yes         (blank)         
         no            yes
         yes           no            
         no          (blank)         
       (blank)         yes           
       (blank)         no            
       (blank)       (blank)         

10. Apply your _exclusion_ criteria to the papers that have 'yes' in
_both_ the #colname(include title) and #colname(include abstract) columns.  Write
'yes' if one of the exclusion criteria applies; write 'no' if none apply.

    Remember: exclusion criteria apply to papers that _pass_ the
    inclusion criteria.  As such, there should be no answer in the
    #colname(exclude) column for any paper that has 'no' in either the
    #colname(include title) or #colname(include abstract) columns.

    Following are the acceptable combinations of these three columns:
    
     include title   include abstract   exclude
    --------------- ------------------ ---------
          yes           yes               no
          yes           yes               yes
          yes           no              (blank)
          no          (blank)           (blank)
    


11. Commit #filename(papers.csv), and your revised #filename(review_protocol.yml) and/or
#filename(research_question.yml), files by the 23:59 Friday,
#cw_paper_list_due.

_**CAUTION**_: some spreadsheet programs insert a blank line at the
top of CSV files; this will confuse the grading scripts, so you
**must** double-check your #filename(papers.csv) file before
submitting, to ensure the column headings are on the _first_ line.
